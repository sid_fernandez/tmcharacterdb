﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TwinMaskDB.Models.TMModels
{
    public class SkillModel
    {
        public int Id { get; set; }
        public string SkillName { get; set; }
        public string SkillDescription { get; set; }
        public int Cost { get; set; }
        public string PrerequsiteList { get; set; }
        public DateTime LastUpdated { get; set; }
    }
}
