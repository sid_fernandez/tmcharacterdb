﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using TwinMaskDB.Data.TMData;

namespace TwinMaskDB.Models.TMModels
{
    public class CharacterModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Player { get; set; }
        public string Race { get; set; }
        public string Culture { get; set; }
        public string Religion { get; set; }
        public string Email { get; set; }
        public string Backstory { get; set; }
        public string Notes { get; set; }
        public string ChangeLog { get; set; }
        public int Taint { get; set; }
        public int Deaths { get; set; }
        public int Hp { get; set; }
        public int Mana { get; set; }
        public int Cp { get; set; }
        public int CpSpent { get; set; }
        public int Ip { get; set; }
        public int InfAcademic { get; set; }
        public int InfPolitical { get; set; }
        public int InfMilitary { get; set; }
        public int InfEconomic { get; set; }
        public int InfUnderworld{ get; set; }
        public int PostageLocal { get; set; }
        public int PostageOverseas { get; set; }
        public bool IsOpen { get; set; }
        public DateTime LastChanged { get; set; }
        public string Skills { get; set; }
        public string Trainings { get; set; }

        public static string operator -(CharacterModel oldCharacterModel, CharacterModel newCharacterModel)
        {
            var sb = new StringBuilder();
            sb.AppendLine(oldCharacterModel.ChangeLog);
            sb.AppendLine("");
            sb.AppendLine(DateTime.Today.ToString(CultureInfo.InvariantCulture));
            sb.AppendLine("Stat changes");
            sb.AppendLine(string.Format("taint = {0}", newCharacterModel.Taint - oldCharacterModel.Taint));
            sb.AppendLine(string.Format("deaths = {0}", newCharacterModel.Deaths - oldCharacterModel.Deaths));
            sb.AppendLine(string.Format("HP = {0}", newCharacterModel.Hp - oldCharacterModel.Hp));
            sb.AppendLine(string.Format("Mana = {0}", newCharacterModel.Mana - oldCharacterModel.Mana));
            sb.AppendLine(string.Format("CP = {0}", newCharacterModel.Cp - oldCharacterModel.Cp));
            sb.AppendLine(string.Format("CPSpent = {0}", newCharacterModel.CpSpent - oldCharacterModel.CpSpent));
            sb.AppendLine(string.Format("IP = {0}", newCharacterModel.Ip - oldCharacterModel.Ip));
            sb.AppendLine("Influence changes");
            sb.AppendLine(string.Format("Academic = {0}", newCharacterModel.InfAcademic - oldCharacterModel.InfAcademic));
            sb.AppendLine(string.Format("Political = {0}", newCharacterModel.InfPolitical - oldCharacterModel.InfPolitical));
            sb.AppendLine(string.Format("Military = {0}", newCharacterModel.InfMilitary - oldCharacterModel.InfMilitary));
            sb.AppendLine(string.Format("Economic = {0}", newCharacterModel.InfEconomic - oldCharacterModel.InfEconomic));
            sb.AppendLine(string.Format("Underworld = {0}", newCharacterModel.InfUnderworld - oldCharacterModel.InfUnderworld));
            sb.AppendLine("Changes to postage");
            sb.AppendLine(string.Format("Local = {0}", newCharacterModel.PostageLocal - oldCharacterModel.PostageLocal));
            sb.AppendLine(string.Format("Overseas = {0}", newCharacterModel.PostageOverseas - oldCharacterModel.PostageOverseas));
            foreach (var skill in newCharacterModel.Skills.Except(oldCharacterModel.Skills))
            {
                sb.AppendLine(string.Format("Added skill {0}", skill));
            }
            return sb.ToString();
        }

        public static CharacterModel operator +(CharacterModel character, CharacterAdminUpdateModel update)
        {
            character.Taint += update.Taint;
            character.Deaths += update.Deaths;
            character.Cp += update.Cp;
            character.Ip += update.Ip;
            character.InfAcademic += update.InfAcademic;
            character.InfPolitical += update.InfPolitical;
            character.InfMilitary += update.InfMilitary;
            character.InfEconomic += update.InfEconomic;
            character.InfUnderworld += update.InfUnderworld;
            character.PostageLocal += update.PostageLocal;
            character.PostageOverseas += update.PostageOverseas;
            character.IsOpen = true;

            var sb = new StringBuilder();
            sb.AppendLine(character.ChangeLog);
            sb.AppendLine($"{DateTime.Today.Month} Game updates _______________________");
            if (update.Taint != 0)
                sb.AppendLine($"Changed taint by {update.Taint}");
            if (update.Deaths != 0)
                sb.AppendLine($"Died {update.Deaths} times");
            if (update.Cp != 0)
                sb.AppendLine($"Gained {update.Cp} Cp for {update.CpReason}");
            if (update.Ip != 0)
                sb.AppendLine($"Gained {update.Ip} Ip for {update.IpReason}");
            if (update.InfAcademic != 0)
                sb.AppendLine($"Changed Academic influence by {update.InfAcademic}");
            if (update.InfPolitical != 0)
                sb.AppendLine($"Changed Political influence by {update.InfPolitical}");
            if (update.InfMilitary != 0)
                sb.AppendLine($"Changed Military influence by {update.InfMilitary}");
            if (update.InfEconomic != 0)
                sb.AppendLine($"Changed Economic influence by {update.InfEconomic}");
            if (update.InfUnderworld != 0)
                sb.AppendLine($"Changed Underworld influence by {update.InfUnderworld}");
            if (update.PostageLocal != 0)
                sb.AppendLine($"Changed local postage by {update.PostageLocal}");
            if (update.PostageOverseas != 0)
                sb.AppendLine($"Changed overseas postage by {update.PostageOverseas}");
            character.ChangeLog = sb.ToString();
            return character;
        }
    }
}
