﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;
using TwinMaskDB.Data;
using TwinMaskDB.Data.TMData;
using TwinMaskDB.Models;
using TwinMaskDB.Models.TMModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TwinMaskDB.Controllers
{
    public class CharactersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CharactersController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IIdentity LoggedInUser => User.Identity; 
        // GET: /<controller>/
        public IActionResult Index()
        {
            if (LoggedInUser == null)
                return View(new ErrorViewModel());
            var chars = _context.Characters.Where(c => c.Email == LoggedInUser.Name).ToList();
            ViewData["Characters"] = chars;
            if (chars.Any())
                ViewData["Player"] = chars.First().Player;
            return View();
        }

        [HttpPost]
        public IActionResult UpdateCharacter(CharacterModel updatedCharacter, int infAdded)
        {
            //validate the changes made
            if (!ValidateChanges(updatedCharacter, infAdded))
                return RedirectToAction(nameof(Index));
            //update the database
            updatedCharacter.IsOpen = false;
            updatedCharacter.LastChanged = DateTime.Now;
            updatedCharacter.CpSpent = updatedCharacter.Cp - updatedCharacter.Skills
                                           .Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries)
                                           .Select(s => _context.Skills.Single(sc => sc.SkillName == s).Cost)
                                           .Sum();
            updatedCharacter.ChangeLog = _context.Characters.Find(updatedCharacter.Id) - updatedCharacter;
            _context.Characters.Update(updatedCharacter);
            _context.SaveChanges();
            //return to the main character page
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult UpdateBackstory(int charId, string newBackstory)
        {
            var oldChar = _context.Characters.Find(charId);
            if (string.IsNullOrEmpty(oldChar.Backstory))
            {
                oldChar.Backstory = newBackstory;
                _context.Characters.Update(oldChar);
                _context.SaveChanges();
            }
            return RedirectToAction(nameof(Index));
        }

        private bool ValidateChanges(CharacterModel toValidate, int amountAdded = 0)
        {
            var oldChar = _context.Characters.Single(c => c.Id == toValidate.Id);
            if (oldChar == null)
                return false;
            var skillsChanged = toValidate.Skills.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries)
                .Except(oldChar.Skills.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries)).ToList();
            if (skillsChanged.Any())
            {
                var skillsNotTrained = skillsChanged
                    .Except(oldChar.Trainings.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries)).ToList();
                if (skillsNotTrained.Any())
                {
                    var infSpent = oldChar.InfAcademic - toValidate.InfAcademic;
                    infSpent += oldChar.InfEconomic - toValidate.InfEconomic;
                    infSpent += oldChar.InfMilitary - toValidate.InfMilitary;
                    infSpent += oldChar.InfPolitical - toValidate.InfPolitical;
                    infSpent += oldChar.InfUnderworld - toValidate.InfUnderworld;
                    infSpent += amountAdded;
                    if (skillsNotTrained.Count > infSpent)
                        return false;
                }
                var cpCost = skillsChanged.Select(s => _context.Skills.Single(sc => sc.SkillName == s).Cost).Sum();
                if (oldChar.Cp - toValidate.Cp != cpCost)
                    return false;
            }
            if (amountAdded > 0)
            {
                if ((oldChar.Ip - toValidate.Ip) % 3 >= amountAdded)
                    return false;
            }

            return true;
        }
    }
}
