﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TwinMaskDB.Data;
using TwinMaskDB.Data.TMData;
using TwinMaskDB.Models.TMModels;


namespace TwinMaskDB.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class PlayerManagementController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlayerManagementController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewData["Characters"] = _context.Characters.ToList();
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public IActionResult MassUpdateCharacters(List<CharacterAdminUpdateModel> charsToUpdate)
        {
            var result = false;
            foreach (var character in charsToUpdate)
            {
                var updated = _context.Characters.Find(character.Id) + character;
                _context.Characters.Update(updated);
            }
            _context.SaveChanges();
            ViewData["Result"] = result;
            return View();
        }

        public void UploadOldCharacter(string oldCharCSV)
        {
            var parsed = parseOldCharacter(oldCharCSV);
            if (parsed != null)
            {
                _context.Add(parsed);
                _context.SaveChanges();
            }
        }

        public CharacterModel parseOldCharacter(string oldCharCSV)
        {
            var temp = new CharacterModel();
            var splitCSV = oldCharCSV.Split(',', StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < splitCSV.Length; i++)
            {
                switch (splitCSV[i])
                {
                    case "Player:":
                        break;
                    case "Email:":
                        break;
                    case "Character:":
                        break;
                    case "Race:":
                        break;
                    case "Culture:":
                        break;
                    case "Religion:":
                        break;
                    case "Deaths:":
                        break;
                    case "Taint:":
                        break;
                }
            }

            return temp;
        }
    }
}
