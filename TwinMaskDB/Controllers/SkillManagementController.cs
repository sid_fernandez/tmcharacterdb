﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TwinMaskDB.Data;
using TwinMaskDB.Data.TMData;
using TwinMaskDB.Models.TMModels;


namespace TwinMaskDB.Controllers
{
    [Authorize]
    public class SkillManagementController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SkillManagementController(ApplicationDbContext context)
        {
            _context = context;
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            ViewData["Skills"] = _context.Skills.ToList();
            return View();
        }

        [HttpPost]
        public IActionResult CreateSkill(SkillModel newSkill)
        {
            _context.Skills.Add(newSkill);
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult UpdateSkill(SkillModel updatedSkill)
        {
            var oldSkill = _context.Skills.Find(updatedSkill.Id);
            //update all existing character sheets
            var charsToUpdate = _context.Characters.Where(c => c.Skills.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries).Contains(oldSkill.SkillName));
            foreach (var characterModel in charsToUpdate)
            {
                var character = characterModel;
                //if the new skill costs more then add that many cp to the character so they dont go into cp debt
                if (updatedSkill.Cost != oldSkill.Cost)
                {
                    var diff = updatedSkill.Cost - oldSkill.Cost;
                    if (diff > 0)
                    {
                        character.Cp += diff * character.Skills.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries).Count(s => s == oldSkill.SkillName);
                    }
                }
                character.Skills = character.Skills.Replace(oldSkill.SkillName, updatedSkill.SkillName);
//
//                var SkillsExpanded = character.Skills.Split(new[] {':'}, StringSplitOptions.RemoveEmptyEntries);
//                for (int i = 0; i < SkillsExpanded.Length; i++)
//                {
//                    if (SkillsExpanded[i] == oldSkill.SkillName)
//                    {
//                        character.Skills.Replace(oldSkill.SkillName, updatedSkill.SkillName);
//                    }
//                }
            }
            _context.Update(updatedSkill);
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public IActionResult MassCreateSkills(List<SkillModel> skillModels)
        {
            //todo make this work
            return RedirectToAction(nameof(Index));
        }
    }
}
