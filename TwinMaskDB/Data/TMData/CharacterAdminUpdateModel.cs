﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwinMaskDB.Data.TMData
{
    public class CharacterAdminUpdateModel
    {
        public int Id { get; set; }
        public int Taint { get; set; }
        public int Deaths { get; set; }
        public int Cp { get; set; }
        public string CpReason { get; set; }
        public int Ip { get; set; }
        public string IpReason { get; set; }
        public int InfAcademic { get; set; }
        public int InfPolitical { get; set; }
        public int InfMilitary { get; set; }
        public int InfEconomic { get; set; }
        public int InfUnderworld { get; set; }
        public int PostageLocal { get; set; }
        public int PostageOverseas { get; set; }
        public List<string> SkillsTrained { get; set; }
    }
}

/*
 		- taint
		- influence
		- postage
		- death
		- trainings
		- bonus cp with comment
		- ip with comment

 */
