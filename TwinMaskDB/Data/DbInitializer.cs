﻿using System;
using System.Collections.Generic;
using System.Linq;
using TwinMaskDB.Models.TMModels;

namespace TwinMaskDB.Data
{
    public static class DbInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            context.Database.EnsureCreated();
            if (context.Characters.Any() && context.Skills.Any())
            {
                return;
            }

            if (!context.Skills.Any())
            {
                context.Skills.Add(new SkillModel
                {
                    Cost = 1,
                    SkillName = "Test Skill",
                    SkillDescription = "Just a test skill to populate the database initially. just delete this",
                    LastUpdated = DateTime.Today,
                    PrerequsiteList = ""
                });
                context.SaveChanges();
            }

            if (!context.Characters.Any())
            {
                context.Characters.Add(new CharacterModel
                {
                    Name = "test character",
                    Backstory = "",
                    ChangeLog = "",
                    Cp = 0,
                    CpSpent = 0,
                    Culture = "",
                    Deaths = 1,
                    Email = "",
                    Hp = 5,
                    Player = "test player",
                    Race = "noob",
                    Religion = "",
                    Notes = "just delete this character it was just to populate the database",
                    Ip = 0,
                    InfAcademic = 0,
                    InfEconomic = 0,
                    InfMilitary = 0,
                    InfPolitical = 0,
                    InfUnderworld = 0,
                    PostageLocal = 0,
                    PostageOverseas = 0,
                    Mana = 0,
                    Taint = 0,
                    LastChanged = DateTime.Today,
                    IsOpen = false,
                    Skills = "",
                    Trainings = ""
                });
                context.SaveChanges();
            }
        }
    }
}